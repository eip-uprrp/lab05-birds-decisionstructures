#include <QApplication>
#include "mainwindow.h"
#include "bird.h"
#include <cstdlib>
#include <time.h>
#include <QTimer>
#include <QObject>
#include <QDebug>

void FilterBirds(Bird birds[], int N) ;

#include <QTime>
#include <QtMultimedia/QSound>


void birth(MainWindow &w, const Bird &mom, const Bird &dad, Bird &child)
{
    QSound::play( ":bird.wav" );

    //
    // Your code goes here
    //


    // ---- do not modify anything below this line ----
    // add the bird to the scene and show it
    w.addBird(100, 210, child);
    w.show();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    Bird avelardo, juana, piolin ;

    // add juana and avelardo to the scene
    w.addBird(10,10,juana);
    w.addBird(210,10,avelardo);

    // create a timer and call the birth function at the trigger
    QTimer aTimer;
    aTimer.setInterval(1000);
    aTimer.setSingleShot(true);
    aTimer.start();
    QObject::connect(&aTimer,&QTimer::timeout, [&](){ birth(w, juana, avelardo, piolin);});


    w.show();
    return a.exec();
}
