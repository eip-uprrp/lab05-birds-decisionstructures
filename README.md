#Lab. 5: Estructuras de decisión (1)

![](http://demo05.cloudimage.io/s/resize/215/i.imgur.com/3FohqVD.png?1)
![](http://demo05.cloudimage.io/s/resize/215/i.imgur.com/amq0Nv0.png?1)
![](http://demo05.cloudimage.io/s/resize/215/i.imgur.com/HZ6w5nL.png)



En casi todas las instancias en que queremos resolver un problema hay una o más opciones que dependen  de si se cumplen o no ciertas condiciones. Los programas de computadoras se construyen para resolver problemas y, por lo tanto, deben tener una estructura que permita tomar decisiones. En C++ las  instrucciones de decisión (o condicionales) se estructuran utilizando `if`, `else`, `else if` o `switch`. Muchas veces el uso de estas estructuras también envuelve el uso de expresiones de relación y operadores lógicos. En la experiencia de laboratorio de hoy practicaremos el uso de algunas estructuras de decisión completando el diseño de una variante del proyecto *Birds* con el que trabajaste la semana pasada. También repasarás conceptos relacionados a objetos.

##Objetivos:

Al finalizar la experiencia de laboratorio de hoy los estudiantes habrán reforzado su conocimiento y practicado el uso de algunas estructuras de decisión, expresiones de relación y operadores lógicos. También habrán practicado la creación y manipulación de objetos, y la invocación de "setters" y "getters".




##Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los conceptos relacionados a estructuras de decisión.

2. haber repasado los conceptos básicos relacionados a objetos y clases en C++

3. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

------------------------------------------

Para facilitar la ejecución de esta experiencia de laboratorio, a continuación repetimos algunos conceptos relacionados a objetos y la clase `Birds` que estudiamos en el laboratorio anterior.

------------------------------------------

##Clases y Objetos en C++

Al igual que cada variable tiene un *tipo* de dato asociada a ella, cada objeto tiene una *clase* asociada que describe las propiedades del objeto:
sus datos (*atributos*), y los procedimientos que se pueden hacer a los datos (*métodos*).

Para definir y utilizar un objeto  no hay que saber todos los detalles de los métodos del objeto pero hay que saber que datos contiene, que procedimientos se pueden hacer y como interactuar con el objeto. Esta información está disponible en la documentación de la clase. Antes de crear objetos de cualquier clase debemos familiarizarnos con su documentación. La documentación nos indica, entre otras cosas, que ente se está tratando de representar con la clase, y cuales son los interfaces  o funciones disponibles para manipular los objetos de la clase.

Dale un vistazo a la documentación de la clase `Bird` que se encuentra [aquí.](http://ada.uprrp.edu/~ranazario/bird-html/class_bird.html). También estudia el archivo `bird.h` contenido en Moodle.

###Clases

Una clase es un pedazo de código en donde se describe cómo serán los objetos. Se definen los atributos de los datos que contendrá el objeto y las funciones o métodos que hacen algún procedimiento a los datos del objeto. Para declarar una clase debemos especificar los tipos que tendrán las variables y las funciones de la clase. 


Si no se especifica lo contrario, las variables y métodos definidos en una clase serán "privados". Esto quiere decir que esas variables solo se pueden acceder y cambiar por los métodos de la clase (*constructores*, *"setters"* y *"getters"*, entre otras). 

Lo siguiente es el esqueleto de la declaración de una clase:

```
  class NombreClase
   {
    // Declaraciones

    private:
      // Declaraciones de variables miembro o atributos y 
      // prototipos de métodos 
      // que sean privadas para esta clase

      tipo varPrivada;
      tipoDevolver nombreMetodoPrivado(tipo de los parámetros);

    public:
      // Declaraciones de atributos y 
      // prototipos de métodos 
      // que sean públicos para todo el programa

      tipo varPública;
      tipoDevolver nombreMetodoPúblico(tipo de los parámetros);
   };
```


Puedes ver la declaración de la clase `Birds` en el archivo `bird.h` incluido en Moodle.


###Objetos

Un objeto es un ente que contiene datos (al igual que una variable), llamados sus `atributos`, y también contiene procedimientos, llamados `métodos`, que se usan para manipularlos. Los objetos son "instancias" de una clase que se crean de manera similar a como se definen las variables:

`NombreClase nombreObjeto;`

Al crear un objeto tenemos disponibles los métodos de la clase a la que pertenece el objeto.

###Métodos de una clase

En general, en cada clase se definen los prototipos de los métodos para construir los objetos, buscar, manipular y guardar los datos de la clase. 

`tipoDevolver nombreMetodo(tipo de los parámetros);`

Luego, en el código del proyecto se escribe  la función correspondiente al método, comenzando con un encabezado que incluye el nombre de la clase a la cuál pertenece la función:

`TipoDevolver NombreClase::NombreMetodo(parámetros)`

Para que los objetos que sean instancia de una clase puedan tener acceso a las variables privadas de la clase se declaran métodos que sean públicos y que den acceso a estas clases (ver abajo "setters" y "getters"). Es preferible utilizar variables privadas y accederlas mediante los "setters" y "getters" a declararlas públicas, ya que de esta manera el objeto que está asociado a estas variables tiene el control de los cambios que se hacen.

En la experiencia de laboratorio de funciones vimos que para invocar un método escribimos el nombre del objeto, seguido de un punto y luego el nombre del método:

`nombreObjeto.nombreMetodo(argumentos);`

Esta es la manera usual de invocar métodos de una clase. 


####Constructores

Los primeros métodos de una clase que debemos entender son los *constructores*. Estos métodos se invocan automáticamente cuando se crea un objeto con esa clase. Es una manera de inicializar el objeto con algunos valores por defecto para sus atributos. El saber cuáles son los constructores nos ayuda a entender cómo crearemos los objetos de esa clase. 

En C++, los  constructores tienen el mismo nombre que la clase. No se declara el tipo que devuelven porque estas funciones no devuelven ningún valor. Su declaración (incluida en la declaración de la clase) es algo así:

`nombreMetodo(tipo de los parámetros);`

El encabezado de la función será algo así:

`NombreClase::NombreMetodo(parámetros)`

La clase `Birds` que estaremos usando en la sesión de hoy tiene dos constructores (funciones sobrecargadas):

`Bird (QWidget *parent=0)`

`Bird (int, EyeBrowType, QString, QString, QWidget *parent=0)`

Puedes ver las declaraciones de los prototipos de estos métodos en la declaración de la clase `Bird` en el archivo `bird.h` del proyecto. La documentación se encuentra [aqui.](http://ada.uprrp.edu/~ranazario/bird-html/class_bird.html)  El primer constructor, `Bird (QWidget *parent=0)`, es un método  que se puede invocar con uno o ningún argumento. Si al invocarlo no se usa argumento, el parámetro de la función toma el valor 0. 

El constructor de una clase que se puede invocar sin usar argumentos es el *constructor* "*default*" de la clase; esto es, el constructor que se invoca cuando creamos un objeto usando una oración como:

`Bird pitirre;`

Puedes ver los métodos y los parámetros que usan en el archivo `bird.cpp`. Nota que el primer constructor, `Bird (QWidget *parent=0)`, asignará valores aleartorios ("random") a cada uno de los atributos del objeto. Más adelante hay una breve explicación de la función `randInt`.

Dale un vistazo a la documentación del segundo constructor, `Bird (int, EyeBrowType, QString, QString, QWidget *parent=0)`. Esta función requiere cuatro argumentos y tiene un quinto argumento que es opcional porque tiene un valor por defecto. Una manera para usar este constructor es creando un objeto como el siguiente:

`Bird guaraguao(200, Bird::UPSET, "blue", "red");`


####"Setters" ("mutators")

Las clases proveen métodos para modificar los valores de los atributos de un objeto que se ha creado. Estos métodos se llaman "*setters*" o "*mutators*". Usualmente se declara un "setter" por cada atributo que tiene la clase. La clase `Bird` tiene los siguientes "setters":


* `void setEyeColor (QString)` 
* `void setFaceColor (QString)` 
* `void setEyebrow (EyeBrowType)` 
* `void setSize (int)` 

Puedes ver las declaraciones de los métodos en la declaración de la clase `Bird` en  `bird.h` y los métodos en `bird.cpp`. El código en el siguiente ejemplo crea el objeto `bobo` de la clase `Bird` y luego cambia su tamaño a 333.

```
Bird bobo;
bobo.setSize(333);
```


####"Getters" ("accessors")

Las clases también proveen métodos para acceder  ("get") el valor del atributo de un objeto. Estos métodos se llaman "*getters*" o "*accessors*". Usualmente se declara un "getter" por cada atributo que tiene la clase. La clase `Bird` tiene los siguientes "getters":

* `QString   getEyeColor ()` 
* `QString  getFaceColor ()` 
* `int getSize ()` 
* `EyeBrowType getEyebrow ()` 

Puedes ver las declaraciones de los meetodos en la declaración de la clase `Bird` en  `bird.h` y los métodos en `bird.cpp`. El código en el siguiente ejemplo crea el objeto `piolin` de la clase `Bird` e imprime su tamaño.

```
Bird piolin;
cout << piolin.getSize();
```

####Otras funciones o métodos que utilizaremos

**MainWindow:** El archivo `mainwindow.h` contiene la declaración de una clase llamada `MainWindow`. Los objetos que sean instancias de esta clase podrán utilizar los métodos sobrecargados

`void MainWindow::addBird(int x, int y, Bird &b)`  

`void MainWindow::addBird(Bird &b)` 

que añadirán a la pantalla un dibujo del objeto de la clase `Bird` que es recibido como argumento. El código en el siguiente ejemplo crea un objeto `w` de la clase `MainWindow`, crea un objeto `zumbador` de la clase `Bird` y lo añade a la posición (200,200) de la pantalla `w` usando el primer método.

```
MainWindow w;
Bird zumbador;
w.addBird(200,200,zumbador);
```


![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/FH7xwDQ.png)



**!Importante!** No es suficiente crear los objetos `Bird` para que éstos aparezcan en la pantalla. Es necesario usar uno de los  métodos `addBird`  para que el dibujo aparezca.


**randInt:** La clase `Bird` incluye el método

`int Bird::randInt(int min, int max)`

para generar números enteros aleatorios ("random") en el rango [min, max]. El método `randInt` depende de otra función para generar números aleatorios que requiere un primer elemento o *semilla* para ser evaluada. En este proyecto, ese primer elemento se genera con la invocación `srand(time(NULL)) ;`.


## Sesión de laboratorio

### La familia *Birds*

Juana y Abelardo, mamá y papá pájaros, están a punto de tener un bebé al que llamarán Piolín. Al igual que los pájaros reales,  los "pájaros Qt" esperan que sus bebés tengan atributos heredados de su madre y su padre.

Al terminar esta experiencia de laboratorio tu programa creará dos pájaros (Juana y Abelardo) con características aleatorias y un tercer pájaro (Piolín), con características determinadas por las características de sus padres, siguiendo un conjunto de reglas parecidas a la reglas de herencia genética.

#### Reglas de herencia

**Color de ojos**

El bebé siempre hereda el color de ojos de la madre.

**Tamaño**

El tamaño del bebé es el más pequeño entre el tamaño del padre o de la madre.

**Color de la cara**

La dominancia de los genes del color de la cara  está dada por la siguiente lista (en inglés), ordenada desde el color más dominante al color menos dominante:

1. blue
2. green
3. red
4. white
5. yellow

El bebé heredará el color de cara más dominante de los colores de sus padres. Por ejemplo, un bebé cuya madre tiene la cara verde y us padre tiene la cara blanca, tendrá cara verde.

**Eyebrows**

La dominancia de los genes de las cejas está dada por la siguiente lista (en inglés), ordenada desde las cejas más dominantes a las cejas  menos dominantes:

1. Birds::ANGRY
1. Birds::BUSHY
1. Birds::UNI
1. Birds::UPSET

Los genes de las cejas siguen las siguientes reglas:

a. Si ambos padres tienen cejas "angry", el bebé tendrá cejas "unibrow".
b. Si ambos padres tienen cejas "unibrow", el bebee tendrá cejas "upset".
c. En los otros casos, el bebé heredará las cejas más dominantes de las cejas de sus padres. 




###Instrucciones

1.	Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/Lab05-Birds-DecisionStructures.git` para descargar la carpeta `Lab05-Birds-DecisionStructures` a tu computadora.

2.	Marca doble "click" en el archivo `BirthOfABird.pro` para cargar este proyecto a Qt. **Importante:** En la pantalla "Configure Project", selecciona la configuración Qt 5.3 o Qt 5.4 clang 64 bit. Su usas otra configuración el proyecto no compilará.

3. Compila y corre el proyecto. Debes ver una pantalla con dos pájaros que representan a Juana y Abelardo. Luego de un segundo, sereas testigo del nacimiento de su bebee Piolín. Sin embargo, este Piolín pudo ser haber volado de otro nido y no ser su hijo, tendrá características aleatorias.

4. Abre el archivo `main.cpp` (no harás cambios a ningún otro archivo de este proyecto). Estudia la función `main`. NO harás cambios a la función `main` en este ejercicio. Nota que la función `main` escencialmente hace dos cosas:
  a. Crea los tres peajaros y añade dos de ellos a la pantalla.
  b. Crea un cronómetro ("timer") que espera un segundo y luego invoca a la función `birth` pasándole una referencia a la ventana y a los tres pájaros.

    ![](http://demo05.cloudimage.io/s/resize/400/i.imgur.com/EXyFhv3.png)

    **Figura 1.** Función `main` 



5. Estudia la función `birth`. Escribe el código necesario para que el bebé Piolín tenga las características dictadas por las reglas de herencia explicadas anteriormente.

    ![](http://demo05.cloudimage.io/s/resize/400/i.imgur.com/ra7hu1M.png)

    **Figura 2.** Función `birth`




6. Entrega el archivo `main.cpp` [utilizando Moodle](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=7504). Asegúrate de que documentes las funciones y proveas un encabezado para el programa que identifique a los programadores e incluya una descripción del programa.

  

####Referencias:

https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-2


